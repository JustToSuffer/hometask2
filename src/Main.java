import java.util.Random;

public class Main {

    static final int B = 0;
    static final int KB = 1;
    static final int MB = 2;
    static final int GB = 3;
    static final int TB = 4;

    public static void main(String[] args) {

        Random random = new Random();
        int bytes = Math.abs(random.nextInt());
        System.out.println(bytes);
        printBytes(bytes);

    }

    public static void printBytes(double value) {
        int rank = 0;
        String prefix = "";

        while (value > 1024) {
            value /= 1024;
            rank++;
        }

        switch (rank) {
            case B:
                prefix = "B";
                break;

            case KB:
                prefix = "KB";
                break;

            case MB:
                prefix = "MB";
                break;

            case GB:
                prefix = "GB";
                break;

            case TB:
                prefix = "TB";
                break;

        }

        System.out.print(String.format("%.1f", value) + " " + prefix);

    }
}
